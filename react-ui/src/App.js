import React, {useState, useEffect}  from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Cookies from 'js-cookie';

import Nav from './components/Nav/Nav';
import Login from './components/Login/Login';
import Signup from './components/Signup/Signup';
import Home from './pages/Home/Home';
import Admin from './pages/Admin/Admin';
import ResetPasswordForm from './pages/ResetPassword/ResetPasswordForm';
import ResetPassword from './pages/ResetPassword/ResetPassword';


function App() {
  const [authorized, setAuthorized] = useState(false);

useEffect(() => {
  if(Cookies.get('email')){
    setAuthorized(true);
  }else{
    setAuthorized(false);
  }
}, [authorized])

const Authorize = () => {
  setAuthorized(!authorized);
}

  return (
    <div className="App">
      <Router>
      <Nav authorized={authorized}/>
        <Switch>
          <Route path='/login'>
            <Login Authorize={Authorize}/>
          </Route>
          <Route path='/signup'>
            <Signup Authorize={Authorize}/>
          </Route>
          <Route path='/logout'>
            {() => {
              Authorize();
              Cookies.remove('email');
              return <Redirect to='/login' />
            }
            }
          </Route>
          <Route path='/admin'>
            <Admin />
          </Route>
          <Route path='/resetpassword'>
            <ResetPasswordForm />
          </Route>
          <Route path='/reset'>
            <ResetPassword />
          </Route>
          <Route path='/'>
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
