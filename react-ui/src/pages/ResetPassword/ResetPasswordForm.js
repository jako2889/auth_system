import React, {useState} from 'react';
import './ResetPassword.scss';

const ResetPasswordForm = () => {
    //Setting up state
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const HandleSubmit = e => {
        e.preventDefault();

        fetch(`/auth/resetemail`, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json' },
            body: JSON.stringify({email: email})
        })
        .then(res => {
            console.log(res.body);
            setMessage('Email sent. Check your inbox!');
        })
    }

    return (
        <div>
                {
                    message === '' ? (
                        <form>
                            <h3>Send email to reset password</h3>
                            <p>You will get an email with instructions to reset your password.</p>
                            <div className='inputBox'>
                                <input required type='email' placeholder='Write your email..' value={email} onChange={e => setEmail(e.target.value)}/>
                            </div>
                            <button onClick={e => HandleSubmit(e)}>Send email</button>
                        </form>
                    ) : <h3>{message}</h3>
                }
        </div>
    )
}

export default ResetPasswordForm;