import React, {useState} from 'react';
import {useLocation, useHistory} from "react-router-dom";
import './ResetPassword.scss';
import useValidate from '../../functions/useValidate';
import ErrorPopup from '../../components/ErrorPopup/ErrorPopup';

const ResetPassword = () => {
    //Setting up state
    const [password, setPassword] = useState('');

    //Instantiate useLocation with search
    const search = useLocation().search;

    //use history to redirect to login
    let history = useHistory();

    //Validation
    const isValidPassword = useValidate(password, 'password');

    //Get id from query
    const id = new URLSearchParams(search).get('id');

    //Check if ID is null
    if(id === null){
        console.log('No id provided');
        history.push("/login");
    }


    const HandleReset = e => {
        e.preventDefault();

        //Check if ID is not null
        if(!(id === null)){
            //append id and password to body object
            const body = {id: id, password: password};
    
            if(isValidPassword.isValid && id){
                fetch(`/auth/reset`, {
                    method: 'POST',
                    headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json' },
                    body: JSON.stringify(body)
                })
                .then(res => {
                    console.log(res);
                    history.push("/login");
                })
            }else {
                console.log("Not a valid password.");
            }
        }


    }

    return (
        <div>
            <form>
                <h3>Update your password</h3>
                <br />
                <div className='inputBox'>
                    <input required type='password' placeholder='Password..' value={password} onChange={e => setPassword( e.target.value )}/>
                    {!isValidPassword.isValid ? (
                    <div className="error">
                        <ErrorPopup errormsg={isValidPassword.errorMsg}/>
                    </div>
                    ) : ''}
                </div>
                <button onClick={e => HandleReset(e)}>Update password</button>
            </form>
        </div>
    )
}

export default ResetPassword;