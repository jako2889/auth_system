import React, {useEffect, useState} from 'react';
import Cookies from 'js-cookie';
import {useHistory} from 'react-router-dom';
import './Admin.scss';

const Admin = () => {
    //Setting up state
    const [chuckData, setChuckData] = useState([]);

    //Instantiate history for redirecting
    let history = useHistory();

    useEffect(() => {
        if(Cookies.get('email')){
            console.log('logged in..', Cookies.get('email'));

            fetch('http://api.icndb.com/jokes/random/10')
            .then(res => res.json())
            .then(data => {
                //Set state to be equal to data
                setChuckData(data.value);
            })
        }else {
            console.log('not logged in...');
            history.push("/login");
        }
        
    });

    //Fetch api on onClick
    const HandleChuck = e => {
        e.preventDefault();

        fetch('http://api.icndb.com/jokes/random/10')
        .then(res => res.json())
        .then(data => {
            //Set state to be equal to data
            setChuckData(data.value);
        })
    }

    return (
        <div className='Admin'>
            <h3>🦄You just got Chuck Norris'ed 🦄</h3>
            <button onClick={e => HandleChuck(e)}>Click for chuck 🔥</button>
            {chuckData.map(chuckItem => (
                <div key={chuckItem.id} className='chuckItem'>
                    <img src='https://www.placecage.com/gif/400/400' alt='Walrus'/>
                    <p>{chuckItem.joke} ✨</p>
                </div>
            ))}
        </div>
    )
}

export default Admin;