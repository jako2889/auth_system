import React from 'react';
import './Home.scss';
import { Link } from "react-router-dom";

import coverImg from '../../assets/3255469.jpg';

const Home = () => {

    return (
        <div className='Home_wrapper'>
            <div className='header'>
                <div className='header_text'>
                    <h2>Welcome stranger</h2>
                    <p>Please choose to either login or signup.</p>
                    <div className='Links_container'>
                        <Link to='/login'>Login</Link>
                        <p>or</p>
                        <Link to='/signup'>Signup</Link>
                    </div>
                </div>
                <div>
                    <img src={coverImg} alt='Login'/>
                </div>
            </div>
        </div>
    )
}

export default Home;