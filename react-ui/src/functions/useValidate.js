import { useState, useEffect } from 'react';

const useValidate = (value, valueType) => {
    //Setting up state
    const [isValid, setIsValid] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

     useEffect(() => {
         setErrorMsg('');

         //If statement to check if value is valid
         if(value === "" || undefined){
             setIsValid(false);
             setErrorMsg('This field is required.');

         }else if(value.length > 50){
            setIsValid(false);
            setErrorMsg('This field must be less than 50 characters.');

         }else if(valueType === 'email' && !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)){
            setIsValid(false);
            setErrorMsg('This field must an email.');

         }else if(valueType === 'password' && !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/.test(value)){
            setIsValid(false);
            setErrorMsg('Must be between 6 to 20 characters & contain at least one numeric digit, one uppercase and one lowercase letter');

         }else {
             setIsValid(true);
         }
         //append dependencies
     }, [value, valueType]);
        
    //Return bool and msg
    return {isValid, errorMsg};
}

export default useValidate;