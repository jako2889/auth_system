import React, { useState } from 'react';
import './Signup.scss';
import {useHistory} from 'react-router-dom';
import useValidate from '../../functions/useValidate';
import ErrorPopup from '../ErrorPopup/ErrorPopup';

const Signup = props => {
    //Setting up state
    const [value, setvalue] = useState({
        email: '',
        password: ''
    });
    const [error, setError] = useState('');

    //Destructuring props
    const { Authorize } = props;

    //Instantiate history for redirecting
    let history = useHistory();
    
    //Validation
    const isValidEmail = useValidate(value.email, 'email');
    const isValidPassword = useValidate(value.password, 'password');

    const SubmitHandler = e => {
        e.preventDefault();

        if(isValidEmail.isValid && isValidPassword.isValid){
            fetch('/auth/create', {
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json' },
                body: JSON.stringify(value)
            })
            .then(res => res.json())
            .then(data => {
                //Check if data has error msg
                if(!data.message){
                    //Set error to empty string and change to authorized and redirect
                    setError('');
                    Authorize();
                    history.push("/admin");
                }else {
                    setError(data.message);
                }
            })
            .catch(error => {
                console.log('We encounterd an error', error);
            })
        }

    }

    return (
        <div className='signup_form'>
            <form onSubmit={SubmitHandler}>
                <h3>Create account</h3>
                {error ? <h4>{error}</h4> : ''}
                <div className='inputBox'>
                    <input required type='email' placeholder='Email..' value={value.email} onChange={e => setvalue({ ...value, email: e.target.value })}/>
                    {!isValidEmail.isValid ? (
                            <div className="error">
                                <ErrorPopup errormsg={isValidEmail.errorMsg}/>
                            </div>
                        ) : ''}
                </div>
                <div className='inputBox'>
                    <input required type='password' placeholder='Password..' value={value.password} onChange={e => setvalue({ ...value, password: e.target.value })}/>
                    {!isValidPassword.isValid ? (
                            <div className="error">
                                <ErrorPopup errormsg={isValidPassword.errorMsg}/>
                            </div>
                    ) : ''}
                </div>
                <button>Signup</button>
            </form>
        </div>
    )
}

export default Signup;