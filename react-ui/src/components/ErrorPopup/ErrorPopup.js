import React from 'react';
import './ErrorPopup.scss';

const ErrorPopup = props => {
    //Destructuring props
    const {errormsg} = props;
    
    return (
        <div className='errorPopup'>
            <p>{errormsg}</p>
        </div>
    )
}

export default ErrorPopup;