import React from 'react';
import {Link} from 'react-router-dom';
import './Nav.scss';

const Nav = props => {
  //Destructuring props
const {authorized} = props;

    return (
            authorized ? (
              <nav>
                <div>
                  <ul>
                    <li>
                      <Link to="/">Home</Link>
                    </li>
                    <li>
                      <Link onClick={() => {
                        console.log('logout..')
                      }} to="/logout">Logout</Link>
                    </li>
                  </ul>
                </div>
              </nav>
            )
            : (
              <nav>
                <ul>
                  <li>
                    <Link to="/">Home</Link>
                  </li>
                  <li>
                    <Link to="/login">Login</Link>
                  </li>
                  <li>
                    <Link to="/signup">Signup</Link>
                  </li>
                </ul>
              </nav>
            )
          )
    
}

export default Nav;