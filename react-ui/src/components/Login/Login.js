import React, { useState } from 'react';
import './Login.scss';
import {Link, useHistory} from 'react-router-dom';
import useValidate from '../../functions/useValidate';
import ErrorPopup from '../ErrorPopup/ErrorPopup';

const Login = props => {
    //Setting up state
    const [value, setvalue] = useState({
        email: '',
        password: ''
    });

    //Destructuring props
    const { Authorize } = props;

    //Instantiate history for redirecting
    let history = useHistory();

    //Validation
    const isValidEmail = useValidate(value.email, 'email');
    const isValidPassword = useValidate(value.password, 'password');

    const SubmitHandler = e => {
        e.preventDefault();

        if(isValidEmail.isValid && isValidPassword.isValid){
            fetch('/auth/login', {
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json' },
                body: JSON.stringify(value)
            })
            .then(res => res.json())
            .then(data => {
                //Change to authorized and redirect
                Authorize();
                history.push("/admin");
            })
            .catch(error => {
                console.log('We encounterd an error', error);
            })
        }
    }

    return (
        <div className='Login_form'>
            <form onSubmit={SubmitHandler}>
                <h3>Login</h3>
                <div className='inputBox'>
                    <input required type='email' placeholder='Email..' value={value.email} onChange={e => setvalue({ ...value, email: e.target.value })}/>
                    {!isValidEmail.isValid ? (
                    <div className="error">
                        <ErrorPopup errormsg={isValidEmail.errorMsg}/>
                    </div>
                    ) : ''}
                </div>
                <div className='inputBox'>                  
                    <input required type='password' placeholder='Password..' value={value.password} onChange={e => setvalue({ ...value, password: e.target.value })}/>
                    {!isValidPassword.isValid ? (
                    <div className="error">
                        <ErrorPopup errormsg={isValidPassword.errorMsg}/>
                    </div>
                    ) : ''}
                </div>
                <Link to='/resetpassword'>Forgot your password?</Link>
                <button>Login</button>
            </form>
        </div>
    )
}

export default Login;
