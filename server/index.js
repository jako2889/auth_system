const express = require('express');
const app = express();

const session = require('express-session');

//Use json and urlencoded
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//ENV
require('dotenv').config();

app.use(session({
    name: 'token',
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, httpOnly: false}
  }));

//CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const rateLimiter = require("express-rate-limit");

//Rate limit for the whole application w. max 100 req
app.use("/",rateLimiter({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100
}));

//Rate limit for the auth part of application w. max 10 req
app.use("/auth",rateLimiter({
    windowMs: 10 * 60 * 1000, // 15 minutes
    max: 10
}));


//ROUTES
const userRoute = require("./routes/user");
app.use(userRoute);

const PORT = process.env.PORT || 8080;

app.listen(PORT, (error) => {
    if(error) {
        console.log("There is an error running on the server " + error);
    }
    console.log(`Server is running on port ${PORT}`);
})