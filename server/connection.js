const MongoClient = require('mongodb').MongoClient;

require('dotenv').config();

const connectionURI = `mongodb+srv://${process.env.ADMIN_USER_NAME}:${process.env.ADMIN_USER_PASSWORD}@clusterauth.yzydg.mongodb.net/Membership_system?retryWrites=true&w=majority`;

const client = new MongoClient(connectionURI, { useUnifiedTopology: true });

client.connect(error => {
    if(!error){
        //Set every email field to be unique
        client.db("Membership_system").collection("users").createIndex( { 'email': 1}, { unique: true });
        
    }else { 
        return res.sendStatus(501);
    }
})

module.exports = client;