const router = require("express").Router();

//Important for using _id in db
const { ObjectId } = require("mongodb"); 

//Sessions
const session = require('express-session');

//MONGODB connection
const client = require('../connection');

//Encrypting passwords
const bcrypt = require('bcrypt');
const saltRounds = 10;

//Import validate function
const validate = require('../validation/formValidate');


//**FIND A USER
router.post("/auth/login", session({
    name: 'token',
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, httpOnly: false}
  }), (req, res) => {

    const body = req.body;
    console.log(body.email);

    //VALIDATE
    if(!validate(body.email, 'email')){
        return res.sendStatus(400);

    }else if(!validate(body.password, 'password')){
        return res.sendStatus(400);

    }

    client.connect(error => {
        if(!error){
            const collection = client.db("Membership_system").collection("users");

            collection.findOne({ email: body.email}, (error, result) => {
                if(result){
                    bcrypt.compare(body.password, result.password, (err, compareResult) => {
                        if(compareResult){

                                res.cookie('email', body.email);
                                return res.send({
                                    user: {email: body.email}
                                });

                        }else {
                            console.log(body.password);
                            console.log(result.password);
                            
                            console.log('Not the same');
                            return res.sendStatus(404);
                        }
                    });
                }else {
                    return res.sendStatus(404);
                }
            });

        }else if(error){
            console.log('Error occurred while connecting to MongoDB Atlas...\n',error);
            return res.status(400).send({ error, msg: "Error occurred while connecting to MongoDB Atlas..." });
        }
    });
    
});

//**CREATE USER
router.post('/auth/create', (req, res) => {
    const body = req.body;

    //VALIDATE
    if(!validate(body.email, 'email')){
        return res.sendStatus(400);
    
    }else if(!validate(body.password, 'password')){
        return res.sendStatus(400);
    
    }

    bcrypt.hash(body.password, saltRounds, (err, hash) => {
        body.password = hash;
    });

    client.connect(error => {
        if(!error){
            const collection = client.db("Membership_system").collection("users");

            //?? Remember to check if email is in db before inserting
            collection.findOne({ email: body.email}, (error, result) => {
                if(result){
                    return res.status(400).send({
                        message: 'Email already exists.'
                    });
                }else {
                    //INSERT INTO DB
                    collection.insertOne({email: body.email, password: body.password}, (error, result) => {
                        if(result){
                            console.log("1 document inserted");
    
                            const welcomeMail = require('../mailer/welcomeMail');
    
                            welcomeMail(body.email).catch(error => console.log(error));
    
                                    res.cookie('email', req.body.email);
                                    return res.send({
                                        user: {email: body.email}
                                    });
                                    
                        }else {
                            return res.sendStatus(501);
                        }
                        
                    })
                }
            })
                
            
        }else if(error){
            console.log('Error occurred while connecting to MongoDB Atlas...\n',error);
            return res.status(400).send({ error, msg: "Error occurred while connecting to MongoDB Atlas..." });
        }
    })
});


//**SEND EMAIL TO USER WITH RESET PASSWORD PAGE AND ID
router.post('/auth/resetemail', (req, res) => {

        //VALIDATE
        if(!validate(req.body.email, 'email')){
            return res.sendStatus(400);
        }

    client.connect(error => {
        if(!error){
            const collection = client.db("Membership_system").collection("users");

            collection.findOne({ email: req.body.email}, (error, result) => {
                if(result){

                    const resetMail = require('../mailer/resetPassword');
                    
                    resetMail(req.body.email, result._id).catch(error => console.log(error));
                
                    return res.send(req.body.email);

                }else {
                    return res.sendStatus(404);
                }
            });

        }else if(error){
            console.log('Error occurred while connecting to MongoDB Atlas...\n',error);
            return res.status(400).send({ error, msg: "Error occurred while connecting to MongoDB Atlas..." });
        }
    });

})

//**UPDATE AND RESET PASSWORD
router.post('/auth/reset', (req, res) => {
    const body = req.body;
    let hashedPassword = '';

    //VALIDATE
    if(!validate(body.password, 'password')){
        return res.sendStatus(400);
    }

    bcrypt.hash(body.password, saltRounds, (err, hash) => {
        body.password = hash;
        hashedPassword = hash;
        console.log("hashed: ", hashedPassword);
    });

    client.connect(error => {
        if(!error){
            const collection = client.db("Membership_system").collection("users");

            let myquery = { _id: ObjectId(req.body.id)};
            let newValue = {$set: {password: hashedPassword}};

            collection.updateOne(myquery, newValue, function(err, result) {
                if(result){
                    console.log('1 document updated');

                    return res.sendStatus(200);
                }else {
                    return res.sendStatus(404);
                }
              });

        }else if(error){
            console.log('Error occurred while connecting to MongoDB Atlas...\n',error);
            return res.status(400).send({ error, msg: "Error occurred while connecting to MongoDB Atlas..." });
        }
    });

});

module.exports = router;