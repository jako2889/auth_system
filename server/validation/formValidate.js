
const validate = (value, valueType) => {
    //Set bool default as false
    let isValid = false;

    //Do the checks
    if(value === "" || undefined){
        console.log('missing values');
        return isValid = false;

    }else if(value.length > 50){
        console.log('too long');
        return isValid = false;

    }else if(valueType === 'email' && !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)){
        console.log('Field not an email');
        return isValid = false;
        
    }else if(valueType === 'password' && !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/.test(value)){
        console.log('More strong password');
        return isValid = false;

    }else {
        isValid = true;
    }

    //Return bool
    return isValid;
}

module.exports = validate;